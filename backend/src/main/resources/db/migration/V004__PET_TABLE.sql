CREATE TABLE pet (
	id uuid NOT NULL,
	owner_id uuid NOT NULL,
	"name" varchar(100) NOT NULL,
	race varchar(100) NOT NULL,
	CONSTRAINT pets_pk PRIMARY KEY (id),
	CONSTRAINT pets_fk FOREIGN KEY (owner_id) REFERENCES person(id) ON DELETE CASCADE
);

