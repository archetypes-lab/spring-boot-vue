CREATE TABLE person (
	id uuid NOT NULL,
	profile_id int4 NOT NULL,
	rut int4 NULL,
	email varchar(100) NOT NULL,
	first_name varchar(100) NOT NULL,
	last_name varchar(100) NOT NULL,
	birthday date NULL,
	active bool NULL,
	CONSTRAINT persons_pk PRIMARY KEY (id),
	CONSTRAINT persons_email_unique UNIQUE (email),
	CONSTRAINT persons_fk1 FOREIGN KEY (profile_id) REFERENCES profile (id)
);
