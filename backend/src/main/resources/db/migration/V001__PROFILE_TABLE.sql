CREATE TABLE profile (
	id int4 NOT NULL,
	name varchar(50) NOT NULL,
	CONSTRAINT profile_pk PRIMARY KEY (id),
	CONSTRAINT profile_name_unique UNIQUE (name)
);

INSERT INTO profile (id, name) VALUES 
(1, 'Administrator'),
(2, 'User');
