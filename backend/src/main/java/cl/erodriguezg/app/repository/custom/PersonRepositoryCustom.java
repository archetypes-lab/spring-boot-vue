package cl.erodriguezg.app.repository.custom;

import java.util.List;

import cl.erodriguezg.app.dto.PersonFilterDto;
import cl.erodriguezg.app.model.Person;

public interface PersonRepositoryCustom {

    List<Person> findByFiltersPaginated(PersonFilterDto filters, int offset, int pageSize);

}
