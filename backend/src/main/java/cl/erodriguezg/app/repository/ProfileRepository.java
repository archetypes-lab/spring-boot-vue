package cl.erodriguezg.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.erodriguezg.app.model.Profile;

public interface ProfileRepository extends JpaRepository<Profile, Integer> {

}
