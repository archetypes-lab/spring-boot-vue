package cl.erodriguezg.app.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.erodriguezg.app.model.Person;
import cl.erodriguezg.app.repository.custom.PersonRepositoryCustom;

public interface PersonRepository extends JpaRepository<Person, UUID>, PersonRepositoryCustom {
    
}
