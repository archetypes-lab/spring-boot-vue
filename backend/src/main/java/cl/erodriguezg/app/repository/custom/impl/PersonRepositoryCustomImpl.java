package cl.erodriguezg.app.repository.custom.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

import cl.erodriguezg.app.dto.PersonFilterDto;
import cl.erodriguezg.app.model.Person;
import cl.erodriguezg.app.model.QPerson;
import cl.erodriguezg.app.repository.custom.PersonRepositoryCustom;

@Repository
public class PersonRepositoryCustomImpl implements PersonRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Person> findByFiltersPaginated(PersonFilterDto filters, int offset, int pageSize) {
        var qPerson = QPerson.person;

        var query = new JPAQuery<Person>(em)
                .select(qPerson)
                .from(qPerson);

        query.where(this.filtersPredicate(qPerson, filters));
            
        return query.fetch();
    }

    private Predicate filtersPredicate(QPerson qPerson, PersonFilterDto filters) {
        var predicates = new ArrayList<Predicate>();
        if (filters != null) {
            if (filters.getRut() != null) {
                predicates.add(qPerson.rut.eq(filters.getRut()));
            }
            if (filters.getUuid() != null) {
                var uuid = UUID.fromString(filters.getUuid());
                predicates.add(qPerson.id.eq(uuid));
            }
        }
        return ExpressionUtils.allOf(predicates);
    }

}
