package cl.erodriguezg.app.dto;

import java.time.LocalDate;
import java.util.UUID;

import cl.erodriguezg.app.model.Person;
import cl.erodriguezg.app.model.Profile;
import lombok.Data;

@Data
public class PersonDto {

    private UUID id;

    private Integer rut;

    private String email;

    private String firstName;

    private String lastName;

    private LocalDate birthday;

    private boolean active;

    private Integer profileId;

    public static Person mapToEntity(PersonDto dto) {
        var entity = new Person();
        entity.setActive(dto.isActive());
        entity.setBirthday(dto.getBirthday());
        entity.setEmail(dto.getEmail());
        entity.setFirstName(dto.getFirstName());
        entity.setId(dto.getId());
        entity.setLastName(dto.getLastName());

        var profile = new Profile();
        profile.setId(dto.getProfileId());
        entity.setProfile(profile);

        entity.setRut(dto.getRut());

        return entity;
    }

}
