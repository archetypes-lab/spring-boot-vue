package cl.erodriguezg.app.dto;

import lombok.Data;

@Data
public class PersonFilterDto {

    private String uuid;

    private Integer rut;

    private String nameLike;

}
