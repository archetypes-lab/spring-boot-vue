package cl.erodriguezg.app.controllers.api;

import java.util.Map;

import cl.erodriguezg.app.exception.BusinessException;
import lombok.Data;
import lombok.Getter;

@Getter
public class ApiResponse<T> {

    public enum Status {
        OK, BUSINESS_ERROR, ERROR, ACCESS_DENIED
    }

    @Data
    public static class ErrorDetail {
        private final String code;
        private final String message;
        private final Map<String, String> details;
    }

    public static <T> ApiResponse<T> ok(T data) {
        var response = new ApiResponse<T>(Status.OK);
        response.payload = data;
        response.error = null;
        return response;
    }

    public static <T> ApiResponse<T> businessException(BusinessException ex) {
        var response = new ApiResponse<T>(Status.BUSINESS_ERROR);
        response.payload = null;
        response.error = new ErrorDetail(ex.getCode(), ex.getMessage(),  ex.getDetails());
        return response;
    }

    public static <T> ApiResponse<T> error() {
        var response = new ApiResponse<T>(Status.ERROR);
        response.payload = null;
        response.error = new ErrorDetail("internal-error", "internal error",  null);
        return response;
    }

    public static <T> ApiResponse<T> accessDenied() {
        var response = new ApiResponse<T>(Status.ACCESS_DENIED);
        response.payload = null;
        response.error = new ErrorDetail("access-denied", "internal error",  null);
        return response;
    }

    private ApiResponse(Status status) {
        this.status = status;
    }
    
    private final Status status;

    private T payload;

    private ErrorDetail error;

}
