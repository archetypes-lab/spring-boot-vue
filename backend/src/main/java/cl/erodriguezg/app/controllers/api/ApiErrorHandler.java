package cl.erodriguezg.app.controllers.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import cl.erodriguezg.app.exception.BusinessException;

@ControllerAdvice
public class ApiErrorHandler {
    
    private static final Logger log = LoggerFactory.getLogger(ApiErrorHandler.class);


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiResponse<Object>> notExceptedErrorHandler(Exception ex) {
        log.error("not excepted error", ex);
        var apiResponse = ApiResponse.error();
        return new ResponseEntity<>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<ApiResponse<Object>> businessExceptionHandler(BusinessException ex) {
        log.warn("business exception", ex);
        var apiResponse = ApiResponse.businessException(ex);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ApiResponse<Object>> accessDeniedHandler(AccessDeniedException ex) {
        log.warn("access denied", ex);
        var apiResponse = ApiResponse.accessDenied();
        return new ResponseEntity<>(apiResponse, HttpStatus.UNAUTHORIZED);
    }

}
