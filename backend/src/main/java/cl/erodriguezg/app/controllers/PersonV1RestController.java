package cl.erodriguezg.app.controllers;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.erodriguezg.app.controllers.api.ApiResponse;
import cl.erodriguezg.app.dto.PersonDto;
import cl.erodriguezg.app.dto.PersonFilterDto;
import cl.erodriguezg.app.model.Person;
import cl.erodriguezg.app.service.PersonService;
import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api/v1/person")
public class PersonV1RestController {

    private static final Logger log = LoggerFactory.getLogger(PersonV1RestController.class);

    private final PersonService personService;

    public PersonV1RestController(PersonService personService) {
        this.personService = personService;
    }

    @Operation(summary = "Get person by UUID")
    @GetMapping("/{uuid}")
    public ApiResponse<Person> getPersonByUuid(@PathVariable String uuid) {
        log.debug("-> getPersonByUuid. uuid: {}", uuid);
        var person = this.personService.findById(uuid);
        return ApiResponse.ok(person);
    }

    @Operation(summary = "Get all persons")
    @GetMapping("/all")
    public ApiResponse<List<Person>> getAllPersons() {
        log.debug("-> getallPersons");
        var persons = this.personService.findAll();
        return ApiResponse.ok(persons);
    }

    @Operation(summary = "Search filtered persons", description = "Search filtered persons with pagination")
    @GetMapping("/filter")
    public ApiResponse<List<Person>> filterPersonsPaginated(PersonFilterDto filters) {
        log.debug("-> filterPersonsPaginated. filters: {}", filters);
        var persons = this.personService.filterPaginated(filters);
        return ApiResponse.ok(persons);
    }

    @Operation(summary = "Save person", description = "Insert or update a person")
    @PostMapping("/save")
    public ApiResponse<UUID> savePerson(@RequestBody @Valid PersonDto personDto, BindingResult bindingResult) {
        log.debug("-> savePerson. personDto: {}", personDto);

        if (bindingResult.hasErrors()) {
            throw new IllegalArgumentException();
        }

        var uuid = this.personService.save(PersonDto.mapToEntity(personDto));
        return ApiResponse.ok(uuid);
    }

}
