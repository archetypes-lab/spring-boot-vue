package cl.erodriguezg.app.service.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.erodriguezg.app.dto.PersonFilterDto;
import cl.erodriguezg.app.exception.BusinessException;
import cl.erodriguezg.app.model.Person;
import cl.erodriguezg.app.repository.PersonRepository;
import cl.erodriguezg.app.repository.ProfileRepository;
import cl.erodriguezg.app.service.PersonService;

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    private final ProfileRepository profileRepository;

    public PersonServiceImpl(PersonRepository repository, ProfileRepository profileRepository) {
        this.personRepository = repository;
        this.profileRepository = profileRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public Person findById(String uuid) {
        var optional = this.personRepository.findById(UUID.fromString(uuid));
        if (optional.isPresent()) {
            return optional.get();
        }
        throw new BusinessException(
                "not-found-person",
                "the person was not found",
                Map.of("uuid", uuid));
    }

    @Transactional(readOnly = true)
    @Override
    public List<Person> findAll() {
        return this.personRepository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Person> filterPaginated(PersonFilterDto filters) {
        return this.personRepository.findByFiltersPaginated(filters, 0, 0);
    }

    @Transactional(readOnly = false)
    @Override
    public UUID save(Person person) {
        var profileId = person.getProfile().getId();
        var profile = this.profileRepository.getReferenceById(profileId);

        person.setProfile(profile);

        var savedPerson = this.personRepository.save(person);
        return savedPerson.getId();
    }

}
