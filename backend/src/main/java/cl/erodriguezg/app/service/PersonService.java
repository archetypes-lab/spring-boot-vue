package cl.erodriguezg.app.service;

import java.util.List;
import java.util.UUID;

import cl.erodriguezg.app.dto.PersonFilterDto;
import cl.erodriguezg.app.model.Person;

public interface PersonService {

    Person findById(String uuid);

    List<Person> findAll();

    List<Person> filterPaginated(PersonFilterDto filters);

    UUID save(Person person);

}
