package cl.erodriguezg.app.exception;

import java.util.Map;

import lombok.Getter;

@Getter
public class BusinessException extends RuntimeException {

    private final String code;

    private final Map<String, String> details;

    public BusinessException(String code, String message) {
        this(code, message, null);
    }

    public BusinessException(String code, String message, Map<String, String> details) {
        super(message);
        this.code = code;
        this.details = details;
    }

}
