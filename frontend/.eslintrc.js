/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution")

module.exports = {
	root: true,
	extends: ["plugin:vue/vue3-essential", "eslint:recommended", "@vue/eslint-config-prettier"],
	plugins: ["vue", "prettier"],
	rules: {
		"prettier/prettier": [
			"error",
			{
				endOfLine: "auto",
				printWidth: 120,
				semi: false,
				doubleQuote: true,
				jsxSingleQuote: true,
				singleQuote: false,
				useTabs: true,
				tabWidth: 2,
			},
		],
		"vue/no-v-html": 0,
	},
}
