class BusinessError extends Error {
	constructor(code, detail, data) {
		const error = new Error(detail)
		error.code = code
		error.data = data
		return error
	}
}

function mapApiResponse(response) {
	const data = response.data
	if (data.status == "OK") {
		return data.payload
	}
	const ex = data.exception
	if (data.status == "BUSINESS_EXCEPTION") {
		throw new BusinessError(ex.code, ex.detail, ex.data)
	} else {
		throw new Error(ex.detail)
	}
}

export { BusinessError, mapApiResponse }
