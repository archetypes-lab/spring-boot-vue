import axios from "axios"
import { mapApiResponse } from "./utils"

export default class PersonApi {
	async findPersonsFiltered() {
		return axios.get("/api/v1/person/filter").then((response) => mapApiResponse(response))
	}
}
