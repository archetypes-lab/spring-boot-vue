import { parse, format } from "date-fns"

export default class DateTimeUtil {
	static parseBackendLocalDate(localDate) {
		return parse(localDate, "yyyy-MM-dd", new Date())
	}

	static formatBackendLocalDate(localDate) {
		return this.formatDate(this.parseBackendLocalDate(localDate))
	}

	static formatDate(theDate) {
		return format(theDate, "dd/MM/yyyy")
	}

	static formatDateTime(theDate) {
		return format(theDate, "dd/MM/yyyy HH:mm:ss")
	}
}
