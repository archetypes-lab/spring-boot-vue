export default class RutUtil {
	static toFormattedText(rutInteger) {
		if (!rutInteger) {
			return null
		}
		const verificationDigit = RutUtil._generateVerificationDigit(rutInteger)
		let formattedRut = ""
		let rutAux = rutInteger + ""
		while (rutAux.length > 3) {
			formattedRut = "." + rutAux.slice(rutAux.length - 3) + formattedRut
			rutAux = rutAux.substring(0, rutAux.length - 3)
		}
		formattedRut = rutAux + formattedRut
		return formattedRut + "-" + verificationDigit
	}

	static toNumber(textRut) {
		let strRut = textRut.split("-")[0]
		while (strRut.indexOf(".") !== -1) {
			strRut = strRut.replace(".", "")
		}
		return parseInt(strRut, 10)
	}

	static _generateVerificationDigit(rutInteger) {
		const rut = rutInteger + ""
		let sum = 0
		let mul = 2
		for (let i = rut.length - 1; i >= 0; i--) {
			sum = sum + parseInt(rut.charAt(i), 10) * mul
			if (mul === 7) {
				mul = 2
			} else {
				mul++
			}
		}
		const res = sum % 11
		if (res === 1) {
			return "K"
		} else if (res === 0) {
			return "0"
		} else {
			return 11 - res
		}
	}
}
